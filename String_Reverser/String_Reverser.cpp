#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

string reverseParentheses(string str, int length)
{
	// int variables to save the indeces of opening and closing brackets
	// new string variable to save and return the reversed string
	int parenthesisBegin, parenthesisEnd;
	string reversedString;

	// The for-loop loops through the whole string. Whenever it finds a bracket,
	// it saves the current index into one of the two int variables declared above.
	// If it's an opening bracket, the index gets saved into "parenthesisBegin". The indeces of closing
	// brackets get saved into "parenthesisEnd".
	for (int i = 0; i < length; i++)
	{
		if (str[i] == '(')
		{
			parenthesisBegin = i;
		}

		// If the loop found both an opening and closing bracket, the content within
		// the parenthesis is being reversed. After reversing the contents of the parenthesis, 
		// both brackets are deleted and the int variable "length" is reduced by 2.
		// To see the changes, the adjusted string will be shown in the console output.
		else if (str[i] == ')')
		{
			parenthesisEnd = i;
			if (str[parenthesisBegin] == '(' && str[parenthesisEnd] == ')')
			{
				reverse(str.begin() + parenthesisBegin + 1, str.begin() + parenthesisEnd);
				str.erase(parenthesisBegin, 1);
				str.erase(parenthesisEnd - 1, 1);
				length -= 2;
				cout << "Parenthesis found, reversing contents of parenthesis, removing brackets of reversed substring: " << str << endl;
			}

			// After removing 2 brackets (an opening and closing bracket), the function checks
			// if it can find another parenthesis. If it does, it calls the function 
			// recursively and returns the string and length of the string.
			if (str.find('(') != string::npos && str.find(')') != string::npos)
			{
				return reverseParentheses(str, length);
			}
		}
	}

	// Finally, after looping through the string until every parenthesis has been reversed and the brackets removed,
	// a final iteration goes through the string to save the reversed string in a new variable.
	// While saving the new string, any remaining brackets, for example if the amount of brackets 
	// was uneven, or any remaining non-alphanumerical characters will be filtered out. That string is then returned back to the "cout" 
	// request from the main function.
	for (int i = 0; i < length; i++)
	{
		if (isalpha(str[i]))
		{
			reversedString += (str[i]);
		}
	}
	return reversedString;
}

int main()
{
	// string variable for the string to check for parentheses and reverse their contents
	// int variable to save the length of the string
	string word;
	int stringLength;

	cout << "Please enter the string you wish to check. If applicable, contents of parentheses will be reversed: ";
	cin >> word;
	stringLength = word.length();

	if (word.find('(') != string::npos && word.find(')') != string::npos)
	{
		cout << "The reversed string is: " << reverseParentheses(word, stringLength);
	}
	else
	{
		cout << "No parentheses found. No adjustments to string needed. Closing program." << endl;
	}
}

